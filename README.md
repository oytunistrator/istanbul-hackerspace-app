istanbul-hackerspace-app
========================

Istanbul Hackerspace için bir Android uygulaması


Versiyon 1.11:

 * Mekanda kaç kişi olduğu bilgisini veren Mekan butonu. 

Versiyon 1.0:

 * Giriş sayfasında blog başlıkları ve kısa özetleri.
 * Ist Hs'nin harita üzerinde yeri.
 * Hackerspace hakkında bilgi.
 * Ist Hackerspace Sosyal medya linkleri.
    
